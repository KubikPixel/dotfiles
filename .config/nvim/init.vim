" A WebDev config for react.js IDE on NeoVim

" Load plugins with Vim-Plug
call plug#begin(stdpath('data') . '/plugged')
  Plug 'neoclide/coc.nvim', {'branch': 'release'}
  Plug 'codechips/coc-svelte', {'do': 'npm install'}
  Plug 'prettier/vim-prettier', { 'do': 'npm install' }
  Plug 'craigemery/vim-autotag'
  Plug 'majutsushi/tagbar'
  Plug 'scrooloose/nerdtree'
  Plug 'ryanoasis/vim-devicons'
  Plug 'airblade/vim-gitgutter'
  Plug 'francoiscabrol/ranger.vim'
  Plug 'junegunn/fzf'
  Plug 'vim-airline/vim-airline'
  Plug 'nathanaelkane/vim-indent-guides'
  Plug 'jiangmiao/auto-pairs'
  Plug 'tpope/vim-commentary'
  Plug 'editorconfig/editorconfig-vim'
  Plug 'blindFS/vim-taskwarrior'
  Plug 'ap/vim-css-color'
  Plug 'morhetz/gruvbox'

" ??? CoC ???
  Plug 'tpope/vim-haml'
  Plug 'evanleck/vim-svelte'
  Plug 'pangloss/vim-javascript'
  Plug 'HerringtonDarkholme/yats.vim'
  Plug 'nrocco/vim-phplint'
  Plug 'evidens/vim-twig'
  Plug 'rust-lang/rust.vim'
  Plug 'Shougo/context_filetype.vim'

  " if has('nvim')
  "   Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
  " else
  "   Plug 'Shougo/deoplete.nvim'
  "   Plug 'roxma/nvim-yarp'
  "   Plug 'roxma/vim-hug-neovim-rpc'
  " endif
  " let g:deoplete#enable_at_startup = 1

  " Plug 'Shougo/neosnippet.vim'
  Plug 'Shougo/neosnippet-snippets'

  Plug 'neomake/neomake'
call plug#end()

" General
syntax on
filetype plugin indent on
set nocompatible
set encoding=UTF-8
set fileencoding=UTF-8
set ffs=unix,dos,mac

" Basic Options
set autoindent
set noshowmode
set showcmd
set autoread
set linebreak
set nowrap
set textwidth=79

" Map Leader
set timeout
set nottimeout
set timeoutlen=300
let mapleader=','

" Show Infos
set title
set hidden
set showmatch
set relativenumber
set number
set numberwidth=3
set showcmd
set cursorline
set cursorcolumn
set colorcolumn=+1
set listchars=tab:»»,trail:·,nbsp:~,eol:↵,extends:❯,precedes:❮
set list
set fillchars=vert:┃
set lazyredraw

" Tabs & Tabstops
set tabstop=2
set softtabstop=2
set shiftwidth=2
set shiftround
set expandtab

" Folding
set foldenable
set foldnestmax=5
set foldlevelstart=5

set foldmethod=indent
" autocmd FileType vim :set foldmethod=marker<CR>

nnoremap <Space> za
vnoremap <Space> za

function! MyFoldText() " {{{
    let line = getline(v:foldstart)

    let nucolwidth = &fdc + &number * &numberwidth
    let windowwidth = winwidth(0) - nucolwidth - 3
    let foldedlinecount = v:foldend - v:foldstart

    " expand tabs into spaces
    let onetab = strpart('  ', 0, &tabstop)
    let line = substitute(line, '\t', onetab, 'g')

    let line = strpart(line, 0, windowwidth - 2 -len(foldedlinecount))
    let fillcharcount = windowwidth - len(line) - len(foldedlinecount)
    return line . '…' . repeat(" ",fillcharcount) . foldedlinecount . '…' . ' '
endfunction " }}}
set foldtext=MyFoldText()

" Wildmenu
set wildmenu
set wildmode=longest:full
set wildignore+=,*.tmp,*.bak,*~,*.swp,*.swo
set wildignore+=,.git,*.hg,*.svn,.DS_Store,.DS_Store*,DS_Store*
set wildignore+=,*.pyc,*.a,*.o
set wildignore+=,*.ts,*.d,*.d.ts
set wildignore+=,*.bmp,*.gif,*.ico,*.jpeg,*.jpg,*.png,*.tif,*.tiff

" Spell
let g:spellfile_='http://ftp.vim.org/vim/runtime/spell'
nnoremap <Leader>sc :setlocal spell! spelllang=de_ch<CR>
nnoremap <Leader>sd :setlocal spell! spelllang=de_de<CR>
nnoremap <Leader>se :setlocal spell! spelllang=en<CR>

" Color Scheme
set t_Co=256
set background=dark
colorscheme gruvbox
let g:gruvbox_italic=1
if (empty($TMUX))
  if (has("nvim"))
    "For Neovim 0.1.3 and 0.1.4 < https://github.com/neovim/neovim/pull/2198 >
    let $NVIM_TUI_ENABLE_TRUE_COLOR=1
  endif
  "For Neovim > 0.1.5 and Vim > patch 7.4.1799 < https://github.com/vim/vim/commit/61be73bb0f965a895bfb064ea3e55476ac175162 >
  "Based on Vim patch 7.4.1770 (`guicolors` option) < https://github.com/vim/vim/commit/8a633e3427b47286869aa4b96f2bfc1fe65b25cd >
  " < https://github.com/neovim/neovim/wiki/Following-HEAD#20160511 >
  if (has("termguicolors"))
    set termguicolors
  endif
endif

"Cursor
highlight Cursor ctermfg=0 ctermbg=21 guifg=#ffffff guibg=#0000ff

" Search
set ignorecase
set smartcase
set showmatch
set hlsearch
set incsearch
highlight Search term=reverse cterm=reverse ctermfg=0 ctermbg=11 gui=reverse guifg=#000000 guibg=#fff999
highlight IncSearch term=reverse cterm=reverse ctermfg=0 ctermbg=2 gui=reverse guifg=#000000 guibg=#fff000
nnoremap <silent> <Leader>h1 :execute 'match DiffText /\<<C-r><C-w>\>/'<CR>
nnoremap <silent> <Leader>h2 :execute '2match DiffAdd /\<<C-r><C-w>\>/'<CR>
nnoremap <silent> <Leader>h3 :execute '3match DiffChange /\<<C-r><C-w>\>/'<CR>

nnoremap gV `[v`]
nnoremap <Leader>cw :%s/<C-r><C-w>//gn<CR>

" Upper- Lower- TitleCase
function! TwiddleCase(str)
    if a:str ==# toupper(a:str)
        let result = tolower(a:str)
    elseif a:str ==# tolower(a:str)
        let result = substitute(a:str,'\(\<\w\+\>\)', '\u\1', 'g')
    else
        let result = toupper(a:str)
    endif
    return result
endfunction
vnoremap ~ y:call setreg('', TwiddleCase(@"), getregtype(''))<CR>gv""Pgvl

" Deleting Trailing
function! DeleteTrailingWS()
    execute 'normal mz'
    %s/\s\+$//ge
    execute 'normal `z'
endfunc
autocmd BufWrite * :call DeleteTrailingWS()

" Indention And Exdention
vnoremap < <gv
vnoremap > >gv

" Move In Visual Mode
vnoremap <Leader>h dhP`[<C-v>`]
vnoremap <Leader>j djp`[<C-v>`]
vnoremap <Leader>k dkP`[<C-v>`]
vnoremap <Leader>l dlP`[<C-v>`]
vnoremap <Leader>w dwP`[<C-v>`]
vnoremap <Leader>e deP`[<C-v>`]
vnoremap <Leader>ge dgeP`[<C-v>`]
vnoremap <Leader>b dbP`[<C-v>`]
vnoremap <Leader>} d}P`[<C-v>`]
vnoremap <Leader>{ d{P`[<C-v>`]

" Move VISUAL LINE selection within buffer.
xnoremap <silent> K :call wincent#mappings#visual#move_up()<CR>
xnoremap <silent> J :call wincent#mappings#visual#move_down()<CR>

" Textwrap
nnoremap <Leader>tw gqip

" Increment List
set nrformats+=alpha
inoremap <Leader><CR> <Esc>Yp<C-A>WC

" Bullet List
nnoremap <Leader>b <Esc>0i*<Space><Esc>:s/,<Space>/\r-<Space>/g<CR>
nnoremap <Leader>ls <Esc>(V):sort<CR>

" Open URL
let s:urlbrowser = "firefox"
function! HandleURL()
    let s:uri = matchstr(getline("."), '[a-z]*:\/\/[^ >,;]*')
    echo s:uri
    if s:uri != ""
        silent exec "!".s:urlbrowser."'".s:uri."'"
    else
        echo "No URI found in line!"
    endif
endfunction
map <Leader>i :call HandleURL()<CR>

" Open Markdown Viewer
let s:mdviewer = "qmarkdown -d"
function! HandleMD()
  if &filetype == 'markdown'
    silent exec "!".s:mdviewer." %"
  else
    echo "No markdown file open!"
  endif
endfunction
map <Leader>m :call HandleMD()<CR>


" Switch html, twig, Jinja
" (muss noch geschrieben werden)

" Plugin key-mappings.
" Note: It must be "imap" and "smap".  It uses <Plug> mappings.
imap <C-k>     <Plug>(neosnippet_expand_or_jump)
smap <C-k>     <Plug>(neosnippet_expand_or_jump)
xmap <C-k>     <Plug>(neosnippet_expand_target)

" SuperTab like snippets behavior.
" Note: It must be "imap" and "smap".  It uses <Plug> mappings.
"imap <expr><TAB>
" \ pumvisible() ? "\<C-n>" :
" \ neosnippet#expandable_or_jumpable() ?
" \    "\<Plug>(neosnippet_expand_or_jump)" : "\<TAB>"
smap <expr><TAB> neosnippet#expandable_or_jumpable() ?
\ "\<Plug>(neosnippet_expand_or_jump)" : "\<TAB>"

" For conceal markers.
if has('conceal')
  set conceallevel=2 concealcursor=niv
endif

" Prettier Settings
let g:prettier#quickfix_enabled = 0
let g:prettier#autoformat_require_pragma = 0
au BufWritePre *.css,*.svelte,*.pcss,*.html,*.ts,*.js,*.json PrettierAsync

if !exists('g:context_filetype#same_filetypes')
  let g:context_filetype#filetypes = {}
endif

let g:context_filetype#filetypes.svelte =
\ [
\   {'filetype' : 'javascript', 'start' : '<script>', 'end' : '</script>'},
\   {
\     'filetype': 'typescript',
\     'start': '<script\%( [^>]*\)\? \%(ts\|lang="\%(ts\|typescript\)"\)\%( [^>]*\)\?>',
\     'end': '',
\   },
\   {'filetype' : 'css', 'start' : '<style \?.*>', 'end' : '</style>'},
\ ]

let g:ft = ''

" Tell Neosnippet about the other snippets
" ~/.local/share/nvim/vim-snippets
let g:neosnippet#snippets_directory='~/.local/share/nvim/plugged/neosnippet-snippets'

" PHPl -------------------------------------------------------------------- {{{
"g:php_version_id = 800020
"g:php_syntax_extensions_enabled = ["bcmath", "bz2", "core", "curl", "date", "dom", "ereg", "gd", "gettext", "hash", "iconv", "json", "libxml", "mbstring", "mcrypt", "mhash", "mysql", "mysqli", "openssl", "pcre", "pdo", "pgsql", "phar", "reflection", "session", "simplexml", "soap", "sockets", "spl", "sqlite3", "standard", "tokenizer", "wddx", "xml", "xmlreader", "xmlwriter", "zip", "zlib"]
" }}}
" Commentary -------------------------------------------------------------- {{{
map <Leader>c :Commentary<CR>>
" }}}
" NERDTree ---------------------------------------------------------------- {{{
map <Leader>n :NERDTreeToggle<CR>
map <Leader>nf :NERDTreeFind<CR>
" }}}
" Ranger ------------------------------------------------------------------ {{{
map <Leader>r :RangerCurrentDirectory<CR>
" }}}
" Tagbar ------------------------------------------------------------------ {{{
"let g:tagbar_type_solidity = {
"    \ 'ctagstype': 'solidity',
"    \ 'kinds' : [
"        \ 'c:contracts',
"        \ 'e:events',
"        \ 'f:functions',
"        \ 'm:mappings',
"        \ 'v:varialbes',
"    \ ]
"\ }
map <Leader>t :TagbarToggle<CR>
map <Leader>ts :TagbarShowTag<CR>
" }}}
" Indent Guides ----------------------------------------------------------- {{{
let g:indent_guides_auto_colors=1
let g:indent_guides_start_level=2
let g:indent_guides_guide_size=2
autocmd BufRead * IndentGuidesEnable
" }}}
" gitgutter --------------------------------------------------------------- {{{
let g:gitgutter_sign_added = '+'
let g:gitgutter_sign_modified = '~'
let g:gitgutter_sign_removed = '−'
let g:gitgutter_sign_removed_first_line = '_'
let g:gitgutter_sign_modified_removed = '⭤'
" }}}
" airline ----------------------------------------------------------------- {{{
set laststatus=2
let g:airline#extensions#tabline#enabled=1
let g:airline_powerline_fonts=1
if !exists('g:airline_symbols')
    let g:airline_symbols = {}
endif

" unicode symbols
let g:airline_left_sep='»'
let g:airline_left_sep='▶'
let g:airline_right_sep='«'
let g:airline_right_sep='◀'
let g:airline_symbols.crypt='🔒'
let g:airline_symbols.linenr='␊'
let g:airline_symbols.linenr='␤'
let g:airline_symbols.linenr='¶'
let g:airline_symbols.maxlinenr='☰'
let g:airline_symbols.maxlinenr=''
let g:airline_symbols.branch='⎇'
let g:airline_symbols.paste='ρ'
let g:airline_symbols.paste='Þ'
let g:airline_symbols.paste='∥'
let g:airline_symbols.spell='Ꞩ'
let g:airline_symbols.notexists='∄'
let g:airline_symbols.whitespace='Ξ'

" powerline symbols
let g:airline_left_sep=''
let g:airline_left_alt_sep=''
let g:airline_right_sep=''
let g:airline_right_alt_sep=''
let g:airline_symbols.branch=''
let g:airline_symbols.readonly=''
let g:airline_symbols.linenr=''

" theme
let g:airline_theme='gruvbox'
" }}}
